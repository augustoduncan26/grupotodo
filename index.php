<?php require ("framework.php") ;?>
<!DOCTYPE html>
<html>
<head>
	<title>Grupo Todo</title>
	<script src="assets/jquery4/jquery-3.4.min.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap4/css/bootstrap.min.css">
	<script type="text/javascript" src="assets/bootstrap4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <div class="row border p-3 bg-success">
	<div class="col-sm-8"><h1>Grupo Todo: PHP y MySQL</h1></div>
	<div class="col-sm-2 top-10"><h4 class="active">PHP-MySQL</h4></div>
	<div class="col-sm-2 top-10"><h4><a href="index-2.php">Lógica</a></h4></div>
  </div>
  <div class="row border p-5">
		<div class="col-sm">
		Seleccione un mes:
		<select class="select-month wd-20">
			<option></option>
			<option value="4">Abril</option>
			<option value="5">Mayo</option>
		</select>
		</div>
		<div class="col-sm">
		<label class="mssg"></label>
		</div>
  </div>

  <div class="row">
	<table class="table table-bordered table-hover table-list">
		<thead>
		<tr>
			<th>#</th>
			<th>Producto</th>
			<th>Cantidad Vendida</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
  </div>
</div>

<div id="detailProductModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" action="" id="api_crud_form">
		<div class="modal-header">
			<h4 class="text-left modal-title">Detalle de Producto</h4>
			<button type="button" class="close text-right" data-dismiss="modal">&times;</button>
		</div>
		<div class="modal-body">
			<label id="prod-name"></label>
			<div class="form-group">
			<label class="text-left message"></label>
			<br />
				<table>
				</table>
			</div> 
		</div>
		<div class="modal-footer">
			<button class="btn btn-danger btn-xls" data-dismiss="modal">Close</button>
		</div>
	</form>
	</div>
	</div>
</div>

<script src="assets/js/App.js"></script>
</body>
</html>