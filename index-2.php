<?php require ("framework.php") ;?>
<!DOCTYPE html>
<html>
<head>
	<title>Grupo Todo</title>
	<script src="assets/jquery4/jquery-3.4.min.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap4/css/bootstrap.min.css">
    <script type="text/javascript" src="assets/bootstrap4/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="assets/jquery4/jquery-ui.1.12.css">
    <script src="assets/jquery4/jquery-1.12.js"></script>
    <script src="assets/jquery4/jquery-ui.js"></script>
</head>
<body>
<div class="container">
  <div class="row border p-3 bg-success">
	<div class="col-sm-8"><h1>Grupo Todo: Logica de Programación</h1></div>
	<div class="col-sm-2 top-10"><h4><a href="index.php">PHP-MySQL</a></h4></div>
	<div class="col-sm-2 top-10"><h4 class="active">Lógica</h4></div>
  </div>
  <div class="row border p-5">
		<div class="col-sm-5">
		    Fecha pactada: <input type="text" id="fechadesde" class="" >
        </div>
        <div class="col-sm-5">
		    Fecha de devolución: <input type="text" id="fechahasta" class="" >
        </div>
        <!--
        <div class="col-sm-2">
		    <button class="btn btn-outline-success">Calcular</button>
		</div>-->
		<div class="col-sm-12">
		<label class="mssg"></label>
		</div>
  </div>

  <div class="row">
	<table class="table table-bordered table-hover table-list">
		<thead>
		<tr>
			<th colspan="4" id="result-multa"></th>
		</tr>
		<tr>
			<th colspan="4" id="result-days"></th>
		</tr>
		</thead>
		<tbody></tbody>
	</table>
  </div>
</div>

<div id="apicrudModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" action="" id="api_crud_form">
		<div class="modal-header">
			<h4 class="text-left modal-title"></h4>
			<button type="button" class="close text-right" data-dismiss="modal">&times;</button>
		</div>
		<div class="modal-body">
			<div class="form-group">
			<label class="text-left message"></label>
			<br />
			<label>Enter First Name</label>
			<input type="text" autocomplete="off" name="first_name" id="first_name" class="form-control">
			</div>
			<div class="form-group">
			<label>Enter Last Name</label>
			<input type="text" autocomplete="off" name="last_name" id="last_name" class="form-control">
			</div> 
		</div>
		<div class="modal-footer">
			<input type="hidden" id="hidden_id" name="hidden_id">
			<input type="hidden" id="action" name="action" value="insert">
			<input type="submit" name="button_action" id="button_action" class="btn btn-info btn-xs" value="Insert">
			<button class="btn btn-danger btn-xls" data-dismiss="modal">Close</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script src="assets/js/Moment.min.js"></script>
<script src="assets/js/App.js"></script>
</body>
</html>