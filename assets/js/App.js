
/***
 * Functions for Page Logica
 * Esa función deberá devolver .
 * como salida un número entero 
 * con el importe de la multa a cobrar 
 * (un 0 en caso de que no se deba cobrar).
 */
if ($('#fechadesde')[0]) {

    var fullDate    = new Date();
    var onlyMonth   = fullDate.getMonth();
    var onlyYear    = fullDate.getFullYear();

    $(document).ready(function () {
        $( function() {
            $( "#fechadesde , #fechahasta" ).datepicker({
                dateFormat: 'dd-mm-yy'
            });
        } );
    });

    $('#fechadesde , #fechahasta').on('change', function() {

        var fromVal  = $('#fechadesde').val();
        var toVal    = $('#fechahasta').val();

        var newFrom  = transformDate ( fromVal , '-' , 'en' );
        var newTo    = transformDate ( toVal , '-' , 'en' );

        var x        = new Date(newFrom);
        var y        = new Date(newTo);
       
        // Calculate days
        if (toVal !='') { 

            var newFrom2    = transformDate ( fromVal , '-' , 'en' );
            var newTo2      = transformDate ( toVal , '-' , 'en' );
            
            var diasDif     = howManyDays( 'days' , newFrom2 , newTo2 );
            var totalMonths = getMonthsDiff( y , x );
            var totalYears  = getYearsDiff( y , x );
        
            // Option 1
            if ( y.getTime() <= x.getTime() ) {
                /*
                    1) Si el auto se devuelve antes 
                    o en la misma fecha de devolución pactada, 
                    no se cobra ninguna multa. 
                */
                $('#result-multa').html('MULTA A COBRAR = 0');
                var rowCount = $('.table-list >thead >tr').length;
                if ( diasDif < 0) { diasDif = 0;}
                $('#result-days').html('DIAS = ' + diasDif);
            }

            // Option 2
            if ( (y.getTime() > x.getTime()) && ( totalMonths == 0 && totalYears == 0 )) {
                /*
                    Si el auto se devuelve después de la fecha pactada, 
                    pero todavía dentro del mismo mes y año, 
                    se cobra un recargo de $2000 por cada día pasado 
                    luego de la fecha de entrega pactada
                */
                var charge  = parseFloat(2000*diasDif);
                $('#result-multa').html('MULTA A COBRAR = $ ' + charge + ' ');
                $('#result-days').html('DIAS = ' + diasDif);
            }

            // Option 3
            if ( y.getTime() > x.getTime() && ( totalMonths >= 1 && totalYears == 0 )) {
                /*
                     Si el auto se devuelve después del mes pactado 
                     pero dentro del mismo año, 
                     se cobra un recargo de $70000 por cada mes pasado 
                     luego de la fecha de entrega pactada.
                */
                var charge  = parseFloat(7000*totalMonths);
                $('#result-multa').html('MULTA A COBRAR = $ ' + charge + ' ');
                $('#result-days').html('MESES = ' + totalMonths);
            }

            // Option 4
            if ( y.getTime() > x.getTime() && ( totalYears > 0 ) ) {
                /*
                    Si el auto se devuelve después de la fecha pactada, 
                    y después del año pactado, 
                    se cobra un recargo de $900000 por cada año pasado 
                    luego de la fecha de entrega pactada. 
                */
                var charge  = parseFloat(900000*totalYears);
                $('#result-multa').html('MULTA A COBRAR = $ ' + charge + ' ');
                $('#result-days').html('AÑOS = ' + totalYears);
            }

        }

    });

    /*
    * How many, by param
    * param : days or months or years
    * value1 : 
    * value2 :
    * This method i can use it for
    * get hoy many: (months and years)
    * and erase / delete  getMonthsDiff and
    * getYearsDiff :P
    */
    function howManyDays ( param  , value1 , value2 ) {
        var result = false;
        if ( param == 'days' ) {
            var date1 = moment(value1);
            var date2 = moment(value2);
            var diasDif = (date2.diff(date1, 'days'));
            result = diasDif;
        }
        return result;
    }
    /*
    * Methods get Diff from Months
    */
    function getMonthsDiff( value1 , value2 ) {
        var result = 0;
        var optMonth1 = (value1.getMonth()+1);
        var optMonth2 = (value2.getMonth()+1);
        if ( optMonth1 > optMonth2 ) { 
            result = (optMonth1 - optMonth2);
        }
        return result;
    }
    
    /*
    * Methods get Diff from Years
    */
    function getYearsDiff( value1 , value2 ) {
        var result = 0;
        var optYear1 = (value1.getFullYear());
        var optYear2 = (value2.getFullYear());
        result = (optYear1 - optYear2);
        return result;
    }

    /*
    * Transform Date by param and language
    */
    function transformDate ( date , param , order = null ) {
        var     result;
        date    = date.split("-");
        
        switch ( param ) {
            case '/': 
            result  = date[2] + '/' + date[1] + '/'  + date[0];;
            break;
             case '-': 
            result  = date[2] + '-' + date[1] + '-'  + date[0];;
            break;
        }
        return result;
    }
}


/***
 * Functions for PHP-MySQL
 */
if ($('.select-month')[0]) {
document.querySelector('.select-month').addEventListener('change', (e)=>  {
    'use_strict';
    var month   =   e.target.value;
    var action  =   'fetchMonth';
    var name    =   $('.select-month').find('option:selected').text();
    $('.mssg').html('<h3>Resultados para: '+name+' 2019</h3>');
    if (!month == '') {
        $.ajax({
            url: 'framework.php?fetch=' + month,
            type: 'POST',
            cache: false,
            // processData: false,
            data: { TheMonth:month , TheAction:action},
            dataType: 'text',
            success: function ( data ) {
                $('tbody').html( data );
            },
            error: function ( response ) {
            }
        })
    }
});
}
/***
 * View Product Detail Modal
 */
$(document).on('click','.edit', (e)=>{
    var id 		=	e.target.id;
    var action 	=	'edit';
    var ID 		=	$(this).attr('id');
    // console.log(e)
    $('#detailProductModal').modal('show');
    $.ajax({
        url: '',
        type: '',
        cache: false,
        // processData: false,
        data: { },
        dataType: 'json',
        // contentType: false,
        success: function ( response ) {
        },
        error: function ( response ) {
        }
    });
});
  