<?php
/*** 
    My Api class
*/

class API {

	private $connect =  '';
    private $options =	'';
    private $prefix;
    private $error;
    public  $result     = false;
    public  $ini_array  = [];

	// Constructor
	function __construct ( $mysqli = Array() ) {
        $this->ini_array['MYSQL']   =   $mysqli;
        $this->prefix   =   $mysqli['ABREV'];
		$this->databaseConnection( $this->ini_array['MYSQL'] );
	}

	// Connection
	function databaseConnection ( $param ) {
        try {
		$this->options = [
		    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		    PDO::ATTR_EMULATE_PREPARES   => false,
		];

		$this->connect = new PDO ("mysql:host=".$param['HOST'].";dbname=".$param['DB']."",$param['USER'],$param['PASS'],$this->options);
        
        } catch (Exception $e) {
            echo $this->error = "#ERROR~General: No se ha podido conectar a la Base de Datos: ";
            exit;
        }
    }

    // Insert
	public function insert() {
	}
    // Edit
	public function edit ( $id ) {
    }
    // Add
    public function add () {

    }

    public function getProducts ( $request ) {
        $products    =  "SELECT DISTINCT prod.nombre_producto,prod.id_producto FROM `gt_productos` as prod  INNER JOIN gt_pedidos as pe ON prod.id_producto = pe.id_producto WHERE  month(fecha_pedidos) = ".$request;
        $stat 	     =	$this->connect->prepare($products);
        if ($stat->execute()) {
            while ($row = $stat->fetch(PDO::FETCH_ASSOC)) {
				$data[] = $row;
			}
			return $data;
        }
    }

    public function totalVendida ( $request ) {
        $query      =   "SELECT SUM(cantidad_pedidos) as total FROM `gt_pedidos`  WHERE id_producto = ".$request['id_producto']." AND month(fecha_pedidos) = ".$request['fetch']."" ;
        $statement 	=	$this->connect->prepare($query);
		if ($statement->execute()) {
            $this->result = $statement->fetch(PDO::FETCH_ASSOC);
        }
        return $this->result['total'];
    }
}

?>