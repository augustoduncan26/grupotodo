<?php
    $ini_array  =   parse_ini_file("config.ini",true);
    include('class/myApiClass.php');

    $output 	=	'';
    $request    =   [];
    $totalCantidadVendida = 0;
    
    // Instance the Object
    $myApiobj       =   new API ( $ini_array["MYSQL"] );

    if ( isset($_GET['fetch']) ) {

        $response 	=	$myApiobj->getProducts( $_GET['fetch'] );
	    $result 	=	($response);

        $request['fetch']   = $_GET['fetch'];

        if ( count ($result) ) {
            $r = 0;
            foreach ($result as $key => $row) {
                $r++;
                
                $request['id_producto'] =   $row['id_producto'];
                $totalCantidadVendida   +=   $myApiobj->totalVendida ( $request );

                $output .=  '
                    <tr>
                        <td class="text-center">'.$r.'</td>
                        <td>'.$row['nombre_producto'].'</td>
                        <td>'.
                            $myApiobj->totalVendida ( $request )
                        .'</td>
                        <td><button type="button" class="btn btn-info btn-xs edit" id="" > &#128269; View</button></td>
                    </tr>
                ';
                $row['id_producto'] = '';
            }
            
            $output .= '<tr class="font-weight-bold bg-light"><td colspan=2 >CANTIDAD TOTAL</td><td>'.$totalCantidadVendida.'</td><td></td></tr>';

        } else {
            $output .= 	'
                <tr>
                    <td colspan="4" class="text-center">No Data Found</td>
                </tr>
            ';
        }
    }
    echo $output;
    
?>