-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 13, 2019 at 01:33 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `grupo_todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `gt_clientes`
--

CREATE TABLE `gt_clientes` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(50) DEFAULT NULL,
  `apellido_cliente` varchar(50) DEFAULT NULL,
  `telefono_cliente` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `activo_cliente` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gt_clientes`
--

INSERT INTO `gt_clientes` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `telefono_cliente`, `email`, `activo_cliente`) VALUES
(1, 'Augusto', 'Duncan', '000', 'aduncan@gmail.com', 1),
(2, 'Abel', 'Augusto', '90989', 'aa22@gmail.com', 1),
(3, 'Mely', 'Gonzalez', '8765', 'mgonzalez@hotmail.com', 1),
(4, 'Laura', 'Lopez', '43567', 'lalop1960@yahoo.com', 0),
(5, 'Pedro', 'Pablo Puelo', '34567543', 'pedrop34@hotmail.com', 1),
(6, 'Lucy', 'Lamas', '34567', 'lucyl21@gmail.com', 1),
(7, 'Mariano M.', 'Valles', '3454', 'marianovalles@hotmail.com', 1),
(8, 'Pepito', 'Papera', '234564', 'pepa32@yahoo.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gt_pedidos`
--

CREATE TABLE `gt_pedidos` (
  `id_pedido` int(11) NOT NULL,
  `fecha_pedidos` date DEFAULT NULL,
  `cantidad_pedidos` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gt_pedidos`
--

INSERT INTO `gt_pedidos` (`id_pedido`, `fecha_pedidos`, `cantidad_pedidos`, `id_cliente`, `id_producto`) VALUES
(1, '2019-04-03', 5, 1, 6),
(2, '2019-04-06', 6, 2, 6),
(3, '2019-04-08', 4, 3, 6),
(4, '2019-05-08', 4, 4, 1),
(5, '2019-04-08', 6, 4, 5),
(6, '2019-04-03', 5, 1, 6),
(7, '2019-05-23', 6, 2, 2),
(8, '2019-04-08', 10, 3, 5),
(9, '2019-05-05', 4, 4, 4),
(10, '2019-04-03', 15, 3, 5),
(11, '2019-04-12', 5, 1, 5),
(12, '2019-04-03', 6, 2, 4),
(13, '2019-05-12', 10, 3, 3),
(14, '2019-04-03', 4, 4, 2),
(15, '2019-05-06', 3, 3, 1),
(16, '2019-04-06', 5, 1, 6),
(17, '2019-05-03', 6, 2, 2),
(18, '2019-05-03', 12, 3, 3),
(19, '2019-05-03', 4, 4, 4),
(20, '2019-04-03', 11, 2, 5),
(21, '2019-04-01', 5, 1, 6),
(22, '2019-05-12', 6, 2, 2),
(23, '2019-05-12', 10, 3, 3),
(24, '2019-05-13', 4, 4, 4),
(25, '2019-05-05', 12, 1, 5),
(26, '2019-04-13', 5, 1, 5),
(27, '2019-04-03', 6, 2, 4),
(28, '2019-04-03', 9, 3, 3),
(29, '2019-04-10', 4, 4, 2),
(30, '2019-04-01', 8, 5, 6),
(31, '2019-05-10', 1, 5, 6),
(32, '2019-05-09', 1, 4, 6),
(33, '2019-04-11', 1, 3, 6),
(34, '2019-04-16', 1, 7, 7),
(35, '2019-04-02', 1, 8, 7),
(36, '2019-04-02', 2, 3, 7),
(37, '2019-05-05', 1, 1, 8),
(38, '2019-05-07', 1, 4, 8),
(39, '2019-05-08', 1, 6, 8),
(40, '2019-05-07', 2, 3, 8),
(41, '2019-05-03', 2, 2, 9),
(42, '2019-05-01', 1, 1, 9),
(43, '2019-04-30', 1, 4, 9),
(44, '2019-04-21', 1, 6, 9),
(45, '2019-05-14', 2, 7, 2),
(46, '2019-04-13', 1, 3, 3),
(47, '2019-04-20', 1, 8, 5),
(48, '2019-05-10', 1, 4, 7),
(49, '2019-05-07', 1, 2, 8),
(50, '2019-05-11', 1, 3, 9),
(51, '2019-05-06', 2, 5, 2),
(52, '2019-05-09', 1, 6, 7);

-- --------------------------------------------------------

--
-- Table structure for table `gt_productos`
--

CREATE TABLE `gt_productos` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(50) DEFAULT NULL,
  `precio_producto` varchar(10) DEFAULT NULL,
  `activo_producto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gt_productos`
--

INSERT INTO `gt_productos` (`id_producto`, `nombre_producto`, `precio_producto`, `activo_producto`) VALUES
(1, 'Laptop Samsung', '12,000', 1),
(2, 'Calculadora', '9,500', 1),
(3, 'Maqu. de Escribir', '23,400', 1),
(4, 'Lavadora portatil', '18,900', 1),
(5, 'Secador de Cabello - Rojo', '8,955.50', 1),
(6, 'Heladera Philips', '9,080.00', 1),
(7, 'Lavarropas Coventry', '6,700', 1),
(8, 'TV LED Samsung 49\"', '5,750', 1),
(9, 'Notebook Lenovo', '12,000', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gt_clientes`
--
ALTER TABLE `gt_clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indexes for table `gt_pedidos`
--
ALTER TABLE `gt_pedidos`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indexes for table `gt_productos`
--
ALTER TABLE `gt_productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gt_clientes`
--
ALTER TABLE `gt_clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `gt_pedidos`
--
ALTER TABLE `gt_pedidos`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `gt_productos`
--
ALTER TABLE `gt_productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;
